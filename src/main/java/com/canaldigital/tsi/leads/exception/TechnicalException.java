package com.canaldigital.tsi.leads.exception;

public class TechnicalException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TechnicalException(String msg) {
		this(msg, null);
	}

	public TechnicalException(String msg, Throwable cause) {
		super(msg, cause);
	}


}
