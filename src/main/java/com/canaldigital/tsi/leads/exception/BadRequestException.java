package com.canaldigital.tsi.leads.exception;

import com.canaldigital.tsi.leads.common.ResponseCode;

public class BadRequestException extends LeadsException {

	private static final long serialVersionUID = 1L;

	public BadRequestException(ResponseCode responseCode) {
		super(responseCode);
	}

	public BadRequestException(ResponseCode responseCode, Throwable cause) {
		super(responseCode, cause);

	}
}
