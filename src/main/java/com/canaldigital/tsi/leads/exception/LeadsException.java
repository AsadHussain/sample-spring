package com.canaldigital.tsi.leads.exception;

import com.canaldigital.tsi.leads.common.ResponseCode;

public class LeadsException extends RuntimeException{


	private static final long serialVersionUID = 1L;
	private ResponseCode responseCode;

    public LeadsException(ResponseCode responseCode) {
        this(responseCode, null);
    }

	public LeadsException(ResponseCode responseCode, Throwable cause) {
        super(responseCode.getDescription(), cause);
        this.responseCode = responseCode;
    }

    public ResponseCode getError() {
        return responseCode;
    }

    public void setResponseCode(ResponseCode responseCode) {
        this.responseCode = responseCode;
    }
}
