package com.canaldigital.tsi.leads.endpoint;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.canaldigital.tsi.leads.domain.Customer;
import com.canaldigital.tsi.leads.service.LeadsService;

@Path("/customer")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Component
@Api(description = "the leads API", tags = { "Leads" })
public class LeadsController {

	private static final Logger logger = LoggerFactory
			.getLogger(LeadsController.class);

	@Autowired
	private LeadsService leadsService;

	@POST
	@Path("/addConsent")
	@ApiOperation(value = "Updates customers consent in Leads Database", response = Response.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Customer Data is Updated in Leads DB", response = Response.class),
			@ApiResponse(code = 404, message = "Failed to Updated Data in Leads DB") })
	public Response customerUpdate(
			@ApiParam(value = "customer identification and information to update request", required = true) @RequestBody Customer customer) {
		logger.debug("inside customerUpdate... customerRequest.toString():"
				+ customer.toString());

		leadsService.customerUpdate(customer);
		return Response.ok().build();

	}
}
