package com.canaldigital.tsi.leads.endpoint;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import com.canaldigital.tsi.leads.domain.ErrorDTO;
import com.canaldigital.tsi.leads.exception.BadRequestException;
import com.canaldigital.tsi.leads.exception.LeadsException;

public class LeadsExceptionHandler implements ExceptionMapper<LeadsException> {

	@Override
	public Response toResponse(LeadsException exception) {
		ErrorDTO error = new ErrorDTO();
		error.setResponseCode(exception.getError().getCode());
		error.setMessage(exception.getError().getDescription());

		if (exception instanceof BadRequestException) {
			error.setHttpCode(Status.BAD_REQUEST.getStatusCode());
		} else {
			error.setHttpCode(Status.SERVICE_UNAVAILABLE.getStatusCode());
		}

		return Response.status(error.getHttpCode()).entity(error).build();
	}

}
