package com.canaldigital.tsi.leads.adapter.mustang;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Date;
import java.text.MessageFormat;
import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.canaldigital.tsi.common.http.HTTPExecutor;
import com.canaldigital.tsi.common.http.JSONFactory;
import com.canaldigital.tsi.leads.config.LeadsSettings;
import com.canaldigital.tsi.leads.domain.Customer;
import com.canaldigital.tsi.leads.domain.CustomerContact;
import com.canaldigital.tsi.leads.exception.TechnicalException;
import com.fasterxml.jackson.databind.JsonNode;

@Service
public class MustangAdapter {

	private static final Logger logger = LoggerFactory.getLogger(MustangAdapter.class);

	private final LeadsSettings settings;
	private final JSONFactory json;

	@Autowired
	public MustangAdapter(LeadsSettings settings) {
		this.settings = settings;
		HTTPExecutor executor = new HTTPExecutor.Builder().connectionRequestTimeout(settings.getMustangReadTimeout())
				.connectTimeout(settings.getMustangConnectTimeout()).build();
		json = new JSONFactory.Builder() //
				.executor(executor).build();

	}

	public Customer getCustomerInfo(Customer customer) {
		
		logger.debug("Inside Mustang getCustomerInfo. with customer Id {}", customer.toString());
		
		String pattern = settings.getMustangInfoURL();
		String countryCode = customer.getCustomerId().getCountry();
		String customerNumber = customer.getCustomerId().getCustomerNumber();
		URI uri = formatURI(pattern, countryCode, customerNumber);

		JsonNode node = json
				.get(uri.toString()).onException(Exception.class, ex -> {
					throw new TechnicalException(
							"Cannot get customer info of " + countryCode + "-" + customerNumber + " from Mustang", ex);
				}).onHttpError((rsp, d) -> {
					throw new TechnicalException("Cannot get customer info of " + countryCode + "-" + customerNumber
							+ " from Mustang.  Mustang returned: " + rsp.getStatusLine().getStatusCode());
				}).toResponse(JsonNode.class);
		
		initializeCustomerEntity(customer, node);		

		return customer;
	}

	private void initializeCustomerEntity(Customer customer, JsonNode node) {
		CustomerContact contact = new CustomerContact();

		customer.setFirstName(node.path("name") != null ? node.path("name").path("firstName").asText() : null);

		customer.setSurName(node.path("name") != null ? node.path("name").path("lastName").asText() : null);
		
		customer.setBirthDate((node.path("dateOfBirth") != null && !node.path("dateOfBirth").asText().equalsIgnoreCase("null")) ? Date.valueOf(LocalDate.parse(node.path("dateOfBirth").asText()))  : null);
		
		contact.setMailAdress(node.path("email") != null  ?  node.path("email").asText() : null);
		
		contact.setCity(node.path("legalAddress") != null ? node.path("legalAddress").path("city").asText() : null);
		
		contact.setAddressLine1(getAddressLine(node.path("legalAddress")));
		
		contact.setAddressLine2(getAddressLine(node.path("legalAddress")));
		
		contact.setPostalNumber(node.path("legalAddress").path("postCode").asText());
		

		if(customer.getContact() != null) {
			if(customer.getContact().getPhoneNumber1() != null)
				contact.setPhoneNumber1(customer.getContact().getPhoneNumber1());
			if(customer.getContact().getPhoneNumber2() != null)
				contact.setPhoneNumber2(customer.getContact().getPhoneNumber2());
		}
		else {
			//try to getFrom Mustang Response
			contact.setPhoneNumber1(node.path("phone") != null  ?  node.path("phone").asText() : null);
		}

		customer.setContact(contact);
	}
	
	private String getAddressLine(JsonNode node) {
		String addressLine = new String();

		if(node.path("houseNumber") != null)
			addressLine += " House: " + node.path("houseNumber").asText();
		
		if(node.path("street") != null)
			addressLine += " Street: " + node.path("street").asText();
		
		if(node.path("apartment") != null)
			addressLine += " Apartment: " + node.path("apartment").asText();
		
		if(node.path("city") != null)
			addressLine += " City: " + node.path("city").asText();
		
		if(node.path("country") != null)
			addressLine += " Country: " + node.path("country").asText();
		
		return addressLine;
	}

	private URI formatURI(String pattern, String countryCode, String customerNumber) {
		String urlString = MessageFormat.format(pattern, countryCode, customerNumber);

		try {
			return new URI(urlString);
		} catch (URISyntaxException e) {
			throw new IllegalStateException("Cannot create valid URI with pattern " + pattern + " for customer "
					+ customerNumber + " in country " + countryCode);
		}
	}

}
