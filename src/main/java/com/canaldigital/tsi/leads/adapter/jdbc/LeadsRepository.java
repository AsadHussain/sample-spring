package com.canaldigital.tsi.leads.adapter.jdbc;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.canaldigital.tsi.leads.common.LeadsConstants;
import com.canaldigital.tsi.leads.domain.Customer;


@Repository
public class LeadsRepository {
	 private static final Logger logger = LoggerFactory.getLogger(LeadsRepository.class);
	 
	    @PersistenceContext
	    private EntityManager entityManager;

	    @Transactional
	    public void insertOrUpdate(Customer customer) {

	    	customer = setGeneralParameters(customer);

	    	Customer customerInDb = entityManager.find(Customer.class, customer.getCustomerId());

	    	if(customerInDb == null) { //New entry in DB
	    		customer.getConsent().setConsentDate(Date.valueOf(LocalDate.now()));
	    		entityManager.persist(customer);
	    	}
	    	else { // Existing Customer
	    		customer = updateCustomerConsents(customer, customerInDb);

		        entityManager.merge(customer);
	    	}
	    }

		private Customer updateCustomerConsents(Customer customer, Customer customerInDb) {
			logger.debug("Inside updateCustomerConsents");

			Integer consentForEmailDb = customerInDb.getConsent().getConsentForEmail();
    		Integer consentForMailDb = customerInDb.getConsent().getConsentForMail();
    		Integer consentForSmsDb = customerInDb.getConsent().getConsentForSms();
    		Integer consentForTMDb = customerInDb.getConsent().getConsentForTM();
			String consentTextDb = customerInDb.getConsent().getConsentText();

    		if(customer.getConsent() != null) {
				if(customer.getConsent().getConsentForEmail() == null)
    				customer.getConsent().setConsentForEmail(consentForEmailDb);

				if(customer.getConsent().getConsentForMail() == null)
    				customer.getConsent().setConsentForMail(consentForMailDb);
				
				if(customer.getConsent().getConsentForSms() == null)
    				customer.getConsent().setConsentForSms(consentForSmsDb);
				
				if(customer.getConsent().getConsentForTM() == null)
    				customer.getConsent().setConsentForTM(consentForTMDb);

				if(customer.getConsent().getConsentText() == null)
    				customer.getConsent().setConsentText(consentTextDb);

				customer.getConsent().setConsentDate(Date.valueOf(LocalDate.now()));
    		}

    		return customer;
		}

		private Customer setGeneralParameters(Customer customer) {
			logger.debug("Inside setGeneralParameters");
			customer.setCampaignName(LeadsConstants.CAMPAIGNNAME);
			customer.setLeadFromCompany(LeadsConstants.LEAD_FROM_COMPANY);
			customer.setPreviousCustomer(LeadsConstants.PREVIOUS_CUSTOMER);
			customer.setTvProvider(LeadsConstants.TVPROVIDER);
			customer.setAddedDate(Date.valueOf(LocalDate.now()));

			return customer;
		}
}
