package com.canaldigital.tsi.leads;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import com.canaldigital.tsi.core.APIDefinition;
import com.canaldigital.tsi.core.BaseServiceContext;
import com.canaldigital.tsi.core.config.AppVersion;
import com.canaldigital.tsi.leads.adapter.jdbc.LeadsRepository;
import com.canaldigital.tsi.leads.adapter.mustang.MustangAdapter;
import com.canaldigital.tsi.leads.config.DbSettings;
import com.canaldigital.tsi.leads.config.LeadsSettings;
import com.canaldigital.tsi.leads.config.ServiceContextAutoConfig;
import com.canaldigital.tsi.leads.endpoint.LeadsController;
import com.canaldigital.tsi.leads.endpoint.LeadsExceptionHandler;

@Import(ServiceContextAutoConfig.class)
public class ServiceContext extends BaseServiceContext {

	@Autowired
	private AppVersion appVersion;

	@Autowired
	private DbSettings dbSettings;
	
	@Autowired
	private LeadsSettings leadsSettings;

	@Bean
	public DataSource dataSource() {
		DataSource dataSource = new DataSource();
		dataSource.setDriverClassName(dbSettings.getDriverClass());
		dataSource.setUrl(dbSettings.getUrl());
		dataSource.setUsername(dbSettings.getUsername());
		dataSource.setPassword(dbSettings.getPassword());
		dataSource.setTestOnBorrow(true);
		dataSource.setValidationQuery("select 1 from dual");
		dataSource.setLogValidationErrors(true);
		dataSource.setJmxEnabled(true);
		dataSource.setMaxActive(dbSettings.getMaxActive());
		dataSource.setMaxIdle(dbSettings.getMaxIdle());
		return dataSource;
	}

	@Bean
	public ServletRegistrationBean endpointV1() {
		APIDefinition api = new APIDefinition("Nordic Leads Customer API", APIDefinition.Type.INTERNAL, 1, 0)
				.jaxrsResource(LeadsController.class)
				.jaxrsResource(LeadsExceptionHandler.class);
		return createRESTServlet(api);
	}
	
	@Bean
	public LeadsRepository leadsRepository() {
		return new LeadsRepository();
	}
	
	@Bean
	public MustangAdapter mustangAdapter() {
		return new MustangAdapter(leadsSettings);
	}

}
