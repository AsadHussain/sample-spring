package com.canaldigital.tsi.leads.config;

import org.springframework.boot.actuate.autoconfigure.MetricsDropwizardAutoConfiguration;
import org.springframework.boot.autoconfigure.PropertyPlaceholderAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.transaction.TransactionAutoConfiguration;
import org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.canaldigital.tsi.core.config.CustomServiceContextAutoConfiguration;

@Import({PropertyPlaceholderAutoConfiguration.class,
        MetricsDropwizardAutoConfiguration.class,
        EmbeddedServletContainerAutoConfiguration.class,
        JmxAutoConfiguration.class,
        DataSourceAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class,
        FlywayAutoConfiguration.class,
        TransactionAutoConfiguration.class})
@EnableTransactionManagement
@EntityScan({
        "com.canaldigital.tsi.leads.domain",
        "com.canaldigital.tsi.leads.adapter.jdbc"})
@ComponentScan({
		"com.canaldigital.tsi.leads",
		"com.canaldigital.tsi.leads.endpoint",
        "com.canaldigital.tsi.leads.service"})
public class ServiceContextAutoConfig extends CustomServiceContextAutoConfiguration {
}
