package com.canaldigital.tsi.leads.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.canaldigital.tsi.common.config.AppInfo;
import com.canaldigital.tsi.common.config.ConfigFactory;

@Configuration
public class Config {

	@Autowired
	private AppInfo appInfo;

	@Bean
	public LeadsSettings leadsSettings() {
		return ConfigFactory.createConfig(LeadsSettings.class, appInfo);
	}

	@Bean
	public DbSettings dbSettings() {
		return ConfigFactory.createConfig(DbSettings.class, appInfo);
	}
}
