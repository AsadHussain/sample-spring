package com.canaldigital.tsi.leads.config;

import com.canaldigital.tsi.common.config.Default;
import com.canaldigital.tsi.common.config.TSIConfig;
import com.canaldigital.tsi.common.config.TSIProperty;

@TSIConfig
public interface DbSettings {

    String getDriverClass();

    String getUsername();

    String getUrl();

    @TSIProperty(encrypted = true)
    String getPassword();

    @Default("20")
    int getMaxActive();

    @Default("10")
    int getMaxIdle();
}
