package com.canaldigital.tsi.leads.config;

import com.canaldigital.tsi.common.config.Default;
import com.canaldigital.tsi.common.config.TSIConfig;
import com.canaldigital.tsi.common.config.TSIProperty;

@TSIConfig(sectionName = "leads")
public interface LeadsSettings {
	
	@Default("3000")
	@TSIProperty("leads.connect.timeout")
	int getLeadsConnectTimeout();

	@Default("15000")
	@TSIProperty("leads.read.timeout")
	int getLeadsReadTimeout();
	
	@TSIProperty("mustang.info.url")
	String getMustangInfoURL();
	
	@Default("3000")
	@TSIProperty("mustang.connect.timeout")
	int getMustangConnectTimeout();

	@Default("30000")
	@TSIProperty("mustang.read.timeout")
	int getMustangReadTimeout();
}
