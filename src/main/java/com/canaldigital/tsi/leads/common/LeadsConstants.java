package com.canaldigital.tsi.leads.common;

public class LeadsConstants {
	
	public static final String CAMPAIGNNAME = "OldFriends";
	public static final String LEAD_FROM_COMPANY = "CD";
	public static final Integer PREVIOUS_CUSTOMER = 1;	// can't be "YES" as it is number in DB
	public static final String TVPROVIDER = "CD";
	public static final String COUNTRY_CODE_NO = "NO";
	public static final String COUNTRY_CODE_SE = "SE";
	public static final String COUNTRY_CODE_DK = "DK";
	public static final String COUNTRY_CODE_FI = "FI";

}
