package com.canaldigital.tsi.leads.common;

public enum ResponseCode {

    MANDATORY_DATA_MISSING(100, "Mandatory Data Missing"),
    CONSENTS_UPDATE_SUCCESFULL(1, "Consents has been updated Succesfully"), 
    DATA_INTEGRITY_VOILATION (101, "Data Integrity Voilation. Consent values can be (0,1) only"), 
    EXCEPTION_INSERTING_CUSTOMER_DATA (102, "Something went wrong while insert/update customer data. Please check logs."), 
    CUSTOMER_NOT_EXISTS (103, "Cuatomer does not exists in Mustang"),
    INVALID_COUNTRY_CODE (104, "Invalid Country Code")
    ;

    private int code;
    private String description;

    private ResponseCode(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

}
