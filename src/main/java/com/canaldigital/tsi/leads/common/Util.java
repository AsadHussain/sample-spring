package com.canaldigital.tsi.leads.common;


public class Util {

	public static Boolean isNull(String data) {
		return ((data == null) || (data.trim().isEmpty()));
	}
}
