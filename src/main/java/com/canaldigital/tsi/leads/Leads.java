package com.canaldigital.tsi.leads;

import com.canaldigital.tsi.core.CoreRunner;
import com.canaldigital.tsi.leads.config.Config;

public class Leads {

	public static void main(String[] args) {
        new CoreRunner()
        .withServiceSources(Config.class, ServiceContext.class)
        .run(args);
	}
}
