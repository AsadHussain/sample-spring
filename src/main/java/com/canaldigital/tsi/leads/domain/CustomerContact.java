package com.canaldigital.tsi.leads.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Embeddable
public class CustomerContact implements Serializable {

	@Column(name = "ADRESSLINE1")
	private String addressLine1;

	@Column(name = "ADRESSLINE2")
	private String addressLine2;

	@Column(name = "CITY")
	private String city;

	@Column(name = "MAIL_ADRESS")
	private String mailAdress;

	@Column(name = "PHONE_NUMBER_1")
	private String phoneNumber1;

	@Column(name = "PHONE_NUMBER_2")
	private String phoneNumber2;

	@Column(name = "PHONE_NUMBER_3")
	private String phoneNumber3;

	@Column(name = "PHONE_NUMBER_4")
	private String phoneNumber4;

	@Column(name = "PHONE_NUMBER_5")
	private String phoneNumber5;

	@Column(name = "POSTAL_NUMBER")
	private String postalNumber;

	public CustomerContact() {
		// TODO Auto-generated constructor stub
	}

	public CustomerContact(String addressLine1, String addressLine2,
			String city, String mailAdress, String phoneNumber1,
			String phoneNumber2, String phoneNumber3, String phoneNumber4,
			String phoneNumber5, String postalNumber) {
		super();
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		this.mailAdress = mailAdress;
		this.phoneNumber1 = phoneNumber1;
		this.phoneNumber2 = phoneNumber2;
		this.phoneNumber3 = phoneNumber3;
		this.phoneNumber4 = phoneNumber4;
		this.phoneNumber5 = phoneNumber5;
		this.postalNumber = postalNumber;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getMailAdress() {
		return mailAdress;
	}

	public void setMailAdress(String mailAdress) {
		this.mailAdress = mailAdress;
	}

	public String getPhoneNumber1() {
		return phoneNumber1;
	}

	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}

	public String getPhoneNumber2() {
		return phoneNumber2;
	}

	public void setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}

	public String getPhoneNumber3() {
		return phoneNumber3;
	}

	public void setPhoneNumber3(String phoneNumber3) {
		this.phoneNumber3 = phoneNumber3;
	}

	public String getPhoneNumber4() {
		return phoneNumber4;
	}

	public void setPhoneNumber4(String phoneNumber4) {
		this.phoneNumber4 = phoneNumber4;
	}

	public String getPhoneNumber5() {
		return phoneNumber5;
	}

	public void setPhoneNumber5(String phoneNumber5) {
		this.phoneNumber5 = phoneNumber5;
	}

	public String getPostalNumber() {
		return postalNumber;
	}

	public void setPostalNumber(String postalNumber) {
		this.postalNumber = postalNumber;
	}

	@Override
	public String toString() {
		return "CustomerContact [addressLine1=" + addressLine1
				+ ", addressLine2=" + addressLine2 + ", city=" + city
				+ ", mailAdress=" + mailAdress + ", phoneNumber1="
				+ phoneNumber1 + ", phoneNumber2=" + phoneNumber2
				+ ", phoneNumber3=" + phoneNumber3 + ", phoneNumber4="
				+ phoneNumber4 + ", phoneNumber5=" + phoneNumber5
				+ ", postalNumber=" + postalNumber + "]";
	}

}
