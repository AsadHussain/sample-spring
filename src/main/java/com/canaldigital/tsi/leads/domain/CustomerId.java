package com.canaldigital.tsi.leads.domain;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class CustomerId implements Serializable {

	private String customerNumber;
	private String country;
	private String businessUnit;

	public CustomerId() {
	}

	public CustomerId(String customerNumber, String country,String businessUnit) {
		super();
		this.customerNumber = customerNumber;
		this.country = country;
		this.businessUnit = businessUnit;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	@Override
	public String toString() {
		return "CustomerId [customerNumber=" + customerNumber + ", country="
				+ country + " bu=" + businessUnit + "]";
	}

}
