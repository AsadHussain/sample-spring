package com.canaldigital.tsi.leads.domain;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Embeddable
public class CustomerConsents implements Serializable {

	@Column(name = "CONSENT_FOR_EMAIL")
	private Integer consentForEmail;

	@Column(name = "CONSENT_FOR_MAIL")
	private Integer consentForMail;

	@Column(name = "CONSENT_FOR_SMS")
	private Integer consentForSms;

	@Column(name = "CONSENT_FOR_TM")
	private Integer consentForTM;

	@Column(name = "CONSENT_DATE")
	private Date consentDate;

	@Column(name = "CONSENT_IP")
	private String consentIp;

	@Column(name = "CONSENT_LINK")
	private String consentLink;

	@Column(name = "CONSENT_TEXT")
	private String consentText;

	public CustomerConsents() {
	}

	public CustomerConsents(Integer consentForEmail, Integer consentForMail,
			Integer consentForSms, Integer consentForTM, Date consentDate,
			String consentIp, String consentLink, String consentText) {
		super();
		this.consentForEmail = consentForEmail;
		this.consentForMail = consentForMail;
		this.consentForSms = consentForSms;
		this.consentForTM = consentForTM;
		this.consentDate = consentDate;
		this.consentIp = consentIp;
		this.consentLink = consentLink;
		this.consentText = consentText;
	}

	public Integer getConsentForEmail() {
		return consentForEmail;
	}

	public void setConsentForEmail(Integer consentForEmail) {
		this.consentForEmail = consentForEmail;
	}

	public Integer getConsentForMail() {
		return consentForMail;
	}

	public void setConsentForMail(Integer consentForMail) {
		this.consentForMail = consentForMail;
	}

	public Integer getConsentForSms() {
		return consentForSms;
	}

	public void setConsentForSms(Integer consentForSms) {
		this.consentForSms = consentForSms;
	}

	public Integer getConsentForTM() {
		return consentForTM;
	}

	public void setConsentForTM(Integer consentForTM) {
		this.consentForTM = consentForTM;
	}

	public String getConsentIp() {
		return consentIp;
	}

	public void setConsentIp(String consentIp) {
		this.consentIp = consentIp;
	}

	public String getConsentLink() {
		return consentLink;
	}

	public void setConsentLink(String consentLink) {
		this.consentLink = consentLink;
	}

	public String getConsentText() {
		return consentText;
	}

	public void setConsentText(String consentText) {
		this.consentText = consentText;
	}

	public Date getConsentDate() {
		return consentDate;
	}

	public void setConsentDate(Date consentDate) {
		this.consentDate = consentDate;
	}


	@Override
	public String toString() {
		return "CustomerConsents [consentForEmail=" + consentForEmail
				+ ", consentForMail=" + consentForMail + ", consentForSms="
				+ consentForSms + ", consentForTM=" + consentForTM
				+ ", consentDate=" + consentDate + ", consentIp=" + consentIp
				+ ", consentLink=" + consentLink + ", consentText="
				+ consentText + "]";
	}

}
