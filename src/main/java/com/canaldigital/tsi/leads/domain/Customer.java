package com.canaldigital.tsi.leads.domain;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "NORDIC_LEADS")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Customer implements Serializable {

	@EmbeddedId
	private CustomerId customerId;

	private CustomerConsents consent;

	private CustomerContact contact;

	@Column(name = "SURNAME")
	private String surName;

	@Column(name = "BIRTH_DATE")
	private Date birthDate;

	@Column(name = "FIRSTNAME")
	private String firstName;

	@Column(name = "ADDED_DATE")
	private Date addedDate;

	@Column(name = "CALLBACK_DTTM")
	private Timestamp callBackDTTM;

	@Column(name = "CALL_ATTEMPS")
	private Integer callAttemps;

	@Column(name = "CAMPAIGNNAME")
	private String campaignName;

	@Column(name = "CANCELEDDATE")
	private Date cancelDate;

	@Column(name = "CANCELEDREASON")
	private String canceledReason;

	@Column(name = "CONTACT_MADE_BY")
	private String contactMadeBy;

	@Column(name = "CUSTOMER_PRODUCTS")
	private String customerProducts;

	@Column(name = "LAST_CONTACT")
	private Date lastContact;

	@Column(name = "LAST_RESPONSE")
	private String lastResponse;

	@Column(name = "LEADS_STATUS")
	private String leadsStatus;

	@Column(name = "LEAD_FROM_COMPANY")
	private String leadFromCompany;

	@Column(name = "POSSIBLE_ADDONS")
	private String possibleAddons;

	@Column(name = "PREVIOUS_CUSTOMER")
	private Integer previousCustomer;

	@Column(name = "PUBLISHER")
	private String publisher;

	@Column(name = "Q1")
	private String Q1;

	@Column(name = "Q2")
	private String Q2;

	@Column(name = "Q3")
	private String Q3;

	@Column(name = "Q4")
	private String Q4;

	@Column(name = "Q5")
	private String Q5;

	@Column(name = "RESPONSE_REASON")
	private String responseReason;

	@Column(name = "TVPROVIDER")
	private String tvProvider;

	public Customer() {
	}

	public Customer(CustomerId customerId) {
		this.customerId = customerId;
	}

	public Customer(CustomerId customerId, CustomerConsents consent,
			CustomerContact contact, String surName, Date birthDate,
			String firstName, Date addedDate, Timestamp callBackDTTM,
			Integer callAttemps, String campaignName, Date cancelDate,
			String canceledReason, String contactMadeBy,
			String customerProducts, Date lastContact, String lastResponse,
			String leadsStatus, String leadFromCompany, String possibleAddons,
			Integer previousCustomer, String publisher, String q1, String q2,
			String q3, String q4, String q5, String responseReason,
			String tvProvider) {
		super();
		this.customerId = customerId;
		this.consent = consent;
		this.contact = contact;
		this.surName = surName;
		this.birthDate = birthDate;
		this.firstName = firstName;
		this.addedDate = addedDate;
		this.callBackDTTM = callBackDTTM;
		this.callAttemps = callAttemps;
		this.campaignName = campaignName;
		this.cancelDate = cancelDate;
		this.canceledReason = canceledReason;
		this.contactMadeBy = contactMadeBy;
		this.customerProducts = customerProducts;
		this.lastContact = lastContact;
		this.lastResponse = lastResponse;
		this.leadsStatus = leadsStatus;
		this.leadFromCompany = leadFromCompany;
		this.possibleAddons = possibleAddons;
		this.previousCustomer = previousCustomer;
		this.publisher = publisher;
		Q1 = q1;
		Q2 = q2;
		Q3 = q3;
		Q4 = q4;
		Q5 = q5;
		this.responseReason = responseReason;
		this.tvProvider = tvProvider;
	}

	public CustomerId getCustomerId() {
		return customerId;
	}

	public void setCustomerId(CustomerId customerId) {
		this.customerId = customerId;
	}

	public CustomerConsents getConsent() {
		return consent;
	}

	public void setConsent(CustomerConsents consent) {
		this.consent = consent;
	}

	public CustomerContact getContact() {
		return contact;
	}

	public void setContact(CustomerContact contact) {
		this.contact = contact;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

	public Timestamp getCallBackDTTM() {
		return callBackDTTM;
	}

	public void setCallBackDTTM(Timestamp callBackDTTM) {
		this.callBackDTTM = callBackDTTM;
	}

	public Integer getCallAttemps() {
		return callAttemps;
	}

	public void setCallAttemps(Integer callAttemps) {
		this.callAttemps = callAttemps;
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public String getCanceledReason() {
		return canceledReason;
	}

	public void setCanceledReason(String canceledReason) {
		this.canceledReason = canceledReason;
	}

	public String getContactMadeBy() {
		return contactMadeBy;
	}

	public void setContactMadeBy(String contactMadeBy) {
		this.contactMadeBy = contactMadeBy;
	}

	public String getCustomerProducts() {
		return customerProducts;
	}

	public void setCustomerProducts(String customerProducts) {
		this.customerProducts = customerProducts;
	}

	public Date getLastContact() {
		return lastContact;
	}

	public void setLastContact(Date lastContact) {
		this.lastContact = lastContact;
	}

	public String getLastResponse() {
		return lastResponse;
	}

	public void setLastResponse(String lastResponse) {
		this.lastResponse = lastResponse;
	}

	public String getLeadsStatus() {
		return leadsStatus;
	}

	public void setLeadsStatus(String leadsStatus) {
		this.leadsStatus = leadsStatus;
	}

	public String getLeadFromCompany() {
		return leadFromCompany;
	}

	public void setLeadFromCompany(String leadFromCompany) {
		this.leadFromCompany = leadFromCompany;
	}

	public String getPossibleAddons() {
		return possibleAddons;
	}

	public void setPossibleAddons(String possibleAddons) {
		this.possibleAddons = possibleAddons;
	}

	public Integer getPreviousCustomer() {
		return previousCustomer;
	}

	public void setPreviousCustomer(Integer previousCustomer) {
		this.previousCustomer = previousCustomer;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getQ1() {
		return Q1;
	}

	public void setQ1(String q1) {
		Q1 = q1;
	}

	public String getQ2() {
		return Q2;
	}

	public void setQ2(String q2) {
		Q2 = q2;
	}

	public String getQ3() {
		return Q3;
	}

	public void setQ3(String q3) {
		Q3 = q3;
	}

	public String getQ4() {
		return Q4;
	}

	public void setQ4(String q4) {
		Q4 = q4;
	}

	public String getQ5() {
		return Q5;
	}

	public void setQ5(String q5) {
		Q5 = q5;
	}

	public String getResponseReason() {
		return responseReason;
	}

	public void setResponseReason(String responseReason) {
		this.responseReason = responseReason;
	}

	public String getTvProvider() {
		return tvProvider;
	}

	public void setTvProvider(String tvProvider) {
		this.tvProvider = tvProvider;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", consent=" + consent
				+ ", contact=" + contact + ", surName=" + surName
				+ ", birthDate=" + birthDate + ", firstName=" + firstName
				+ ", addedDate=" + addedDate + ", callBackDTTM=" + callBackDTTM
				+ ", callAttemps=" + callAttemps + ", campaignName="
				+ campaignName + ", cancelDate=" + cancelDate
				+ ", canceledReason=" + canceledReason + ", contactMadeBy="
				+ contactMadeBy + ", customerProducts=" + customerProducts
				+ ", lastContact=" + lastContact + ", lastResponse="
				+ lastResponse + ", leadsStatus=" + leadsStatus
				+ ", leadFromCompany=" + leadFromCompany + ", possibleAddons="
				+ possibleAddons + ", previousCustomer=" + previousCustomer
				+ ", publisher=" + publisher + ", Q1=" + Q1 + ", Q2=" + Q2
				+ ", Q3=" + Q3 + ", Q4=" + Q4 + ", Q5=" + Q5
				+ ", responseReason=" + responseReason + ", tvProvider="
				+ tvProvider + "]";
	}

}
