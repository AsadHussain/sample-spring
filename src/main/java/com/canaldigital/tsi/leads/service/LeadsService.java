package com.canaldigital.tsi.leads.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import com.canaldigital.tsi.leads.adapter.jdbc.LeadsRepository;
import com.canaldigital.tsi.leads.adapter.mustang.MustangAdapter;
import com.canaldigital.tsi.leads.common.LeadsConstants;
import com.canaldigital.tsi.leads.common.ResponseCode;
import com.canaldigital.tsi.leads.common.Util;
import com.canaldigital.tsi.leads.domain.Customer;
import com.canaldigital.tsi.leads.exception.BadRequestException;
import com.canaldigital.tsi.leads.exception.LeadsException;
import com.canaldigital.tsi.leads.exception.TechnicalException;

@Component
public class LeadsService {
	private static final Logger logger = LoggerFactory
			.getLogger(LeadsService.class);

	@Autowired
	private LeadsRepository leadsRepository;
	
	@Autowired
	private MustangAdapter mustangAdapter;

	public LeadsService() {
	}

	public LeadsService(LeadsRepository leadsRepository) {
		this.leadsRepository = leadsRepository;
	}

	public void customerUpdate(Customer customer) {
		
		String countryCode = customer.getCustomerId().getCountry();

		if (customer.getCustomerId() == null
				|| Util.isNull(countryCode)
				|| Util.isNull(customer.getCustomerId().getCustomerNumber())
				|| customer.getConsent() == null
				)
			throw new BadRequestException(ResponseCode.MANDATORY_DATA_MISSING);
		
		if(!(countryCode.equalsIgnoreCase(LeadsConstants.COUNTRY_CODE_NO)
				||countryCode.equalsIgnoreCase(LeadsConstants.COUNTRY_CODE_FI)
				|| countryCode.equalsIgnoreCase(LeadsConstants.COUNTRY_CODE_SE)
				|| countryCode.equalsIgnoreCase(LeadsConstants.COUNTRY_CODE_DK))
				)
			throw new BadRequestException(ResponseCode.INVALID_COUNTRY_CODE);
		
		customer.getCustomerId().setBusinessUnit("CD"+countryCode.toUpperCase());

		try {
			customer = mustangAdapter.getCustomerInfo(customer);
			leadsRepository.insertOrUpdate(customer);
		}
		catch (DataIntegrityViolationException dive) {
			logger.error("DataIntegrityViolationException while inserting/updating customer consent in db: {} " , dive.getMessage());
			throw new BadRequestException(ResponseCode.DATA_INTEGRITY_VOILATION);
		}
		catch (TechnicalException te) {
			throw new LeadsException(ResponseCode.CUSTOMER_NOT_EXISTS);
		}
		catch (Exception ex) {
			logger.error("Exception while inserting/updating customer consent in db: {}" , ex.getMessage());
			throw new BadRequestException(ResponseCode.EXCEPTION_INSERTING_CUSTOMER_DATA);			
		}
	}

	public LeadsRepository getLeadsRepository() {
		return leadsRepository;
	}

	public void setLeadsRepository(LeadsRepository leadsRepository) {
		this.leadsRepository = leadsRepository;
	}

}
